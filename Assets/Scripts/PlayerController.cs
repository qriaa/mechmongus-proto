using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerController : MonoBehaviour
{

    public PlayerControls controlScheme;
    public float movementSpeed;
    public float rotationSpeed;
    public UnityEvent shootEvent;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(controlScheme.forwardKey))
            moveForward();
        if (Input.GetKey(controlScheme.backwardKey))
            moveBackward();
        if (Input.GetKey(controlScheme.leftKey))
            rotateLeft();
        if (Input.GetKey(controlScheme.rightKey))
            rotateRight();
        if (Input.GetKeyDown(controlScheme.shootKey))
            shoot();
    }


    void moveForward()
    {
        transform.position += transform.right * movementSpeed * Time.deltaTime;
    }

    void moveBackward()
    {
        transform.position -= transform.right * movementSpeed * Time.deltaTime;
    }

    void rotateLeft()
    {
        transform.Rotate(0, 0, rotationSpeed * Time.deltaTime);
    }

    void rotateRight()
    {
        transform.Rotate(0, 0, -rotationSpeed * Time.deltaTime);
    }

    void shoot()
    {
        shootEvent.Invoke();
    }
}
