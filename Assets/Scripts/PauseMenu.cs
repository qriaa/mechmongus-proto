using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class PauseMenu : MonoBehaviour
{
    public static bool gameIsPaused  = false;


    public int menuSceneNumber;
    public GameObject pauseMenuUI;
    public GameObject finishMenuUI;
    public AudioClip pauseSound;
    public AudioClip resumeSound;
    public AudioSource audioSource;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            if(gameIsPaused)
            {
                Resume();
            } else
            {
                Pause();
            }
        }
    }

    public void Resume()
    {
        audioSource.PlayOneShot(resumeSound);
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1.0f;
        gameIsPaused = false;
    }
    void Pause()
    {
        audioSource.PlayOneShot(pauseSound);
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0.0f;
        gameIsPaused = true;
    }

    public void Quit()
    {
        Time.timeScale = 1.0f;
        Debug.Log("Qutting");
        Application.Quit();
    }

    public void Menu()
    {
        Time.timeScale = 1.0f;
        SceneManager.LoadScene(menuSceneNumber);
    }
    public void Restart()
    {
        Time.timeScale = 1.0f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Finish(int playerNumber)
    {
        Time.timeScale = 0.0f;
        finishMenuUI.SetActive(true);
        finishMenuUI.transform.GetChild(3).GetComponent<TMP_Text>().text = "PLAYER " + playerNumber + " WON";
    }
}
