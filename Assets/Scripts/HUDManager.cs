using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDManager : MonoBehaviour
{

    public GameObject player1HUD;
    public GameObject player2HUD;
    public Sprite emptyHeart;
    private int player1Tracker;
    private int player2Tracker;

    private void Start()
    {
        player1Tracker = 3;
        player2Tracker = 3;
    }
    public void removeHeart(int playerNumber)
    {
        if(playerNumber == 1)
        {
            if (player1Tracker > 0)
            {
                player1HUD.transform.GetChild(player1Tracker).GetComponent<Image>().sprite = emptyHeart;
                player1Tracker--;
            }
        }
        else if (playerNumber == 2)
        {
            if (player2Tracker > 0)
            {
                player2HUD.transform.GetChild(player2Tracker).GetComponent<Image>().sprite = emptyHeart;
                player2Tracker--;
            }
        }
    }
}
