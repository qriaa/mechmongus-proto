using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumeSettings
{
    private static float volumeLevel = 0.5f;

    public static void ChangeVolumeLevel(float level)
    {
        volumeLevel = level;
    }

    public static float GetVolumeLevel()
    {
        return volumeLevel;
    }
}
