using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SliderScript : MonoBehaviour
{
    [SerializeField] private Slider _slider;


    void Start()
    {
        _slider.onValueChanged.AddListener((v) =>
        {
            VolumeSettings.ChangeVolumeLevel(v);
        });
    }

    private void OnEnable()
    {
        _slider.value = VolumeSettings.GetVolumeLevel();
    }

}
