using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrelBehaviour : MonoBehaviour
{
    int life = 3;
    public Sprite damaged1;
    public Sprite damaged2;
    public SpriteRenderer spriteRender;

    public AudioClip hitSound;
    public GameObject soundPrefab;

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Projectile")
        {
            Destroy(collision.gameObject);
            life--;
            GameObject sound = Instantiate(soundPrefab);
            Sound soundBehavior = sound.GetComponent<Sound>();
            soundBehavior.audioClip = hitSound;
            soundBehavior.playSoundAndDie();
            if (life == 2)
            {
                spriteRender.sprite = damaged1;
            }
            else if (life == 1)
            {
                spriteRender.sprite = damaged2;
            }
            else if( life <= 0 )
            {
                Destroy(gameObject);
            }
        }
    }
}
