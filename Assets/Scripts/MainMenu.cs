using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public int gameSceneNumber;
    public void PlayGame()
    {
        SceneManager.LoadScene(gameSceneNumber);
    }

    public void QuitGame()
    {
        Debug.Log(VolumeSettings.GetVolumeLevel());
        Debug.Log("Quit");
        Application.Quit();
    }
}
