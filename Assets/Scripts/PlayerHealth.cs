using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    // Start is called before the first frame update
    public int health;

    float lastTimeHit;
    public float invulnerabilityTime;
    public GameObject HUD;
    public GameObject UI;
    public int playerNumber;

    public AudioClip hitSound;
    public AudioClip deathSound;

    public GameObject soundPrefab;

    private SpriteRenderer spriteRenderer;

    void Start()
    {
        lastTimeHit = Time.time;
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator invFlash()
    {
        int flashNum = 6;
        for(int i=0; i < flashNum; i++)
        {
            spriteRenderer.enabled = false;
            yield return new WaitForSeconds(invulnerabilityTime / (flashNum*2));
            spriteRenderer.enabled = true;
            yield return new WaitForSeconds(invulnerabilityTime / (flashNum*2));
        }
        
    }

    public void getHit(int damage)
    {
        if(Time.time - lastTimeHit > invulnerabilityTime) {
            StartCoroutine(invFlash());
            lastTimeHit = Time.time;
            health -= damage;
            AudioSource.PlayClipAtPoint(hitSound, transform.position);
            if (health <= 0)
                die();
            HUD.GetComponent<HUDManager>().removeHeart(playerNumber);
        }
    }

    void die()
    {
        GameObject sound = Instantiate(soundPrefab);
        Sound soundBehavior = sound.GetComponent<Sound>();
        soundBehavior.audioClip = deathSound;
        soundBehavior.playSoundAndDie();
        int winningNumber = 1;
        if(playerNumber == 1)
        {
            winningNumber = 2;
        }
        UI.GetComponent<PauseMenu>().Finish(winningNumber);

    }
}
