using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooting : MonoBehaviour
{
    public GameObject projectile;
    public AudioClip shootSound;

    public GameObject soundPrefab;

    float lastTimeShot;
    public float fireCooldown;

    void Start()
    {
        lastTimeShot = Time.time;
    }

    public void shoot()
    {
        if (Time.time - lastTimeShot > fireCooldown)
        {
            lastTimeShot = Time.time;
            GameObject sound = Instantiate(soundPrefab);
            Sound soundBehavior = sound.GetComponent<Sound>();
            soundBehavior.audioClip = shootSound;
            soundBehavior.playSoundAndDie();

            GameObject newProjectile = Instantiate(projectile, transform.position, Quaternion.Euler(transform.localEulerAngles));
            newProjectile.GetComponent<Projectile>().shotAuthor = gameObject;
        }
    }
        
}
