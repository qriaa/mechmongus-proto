using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float projectileSpeed;
    public float expirationLength;
    public int damage;
    public GameObject shotAuthor;
    // Start is called before the first frame update
    void Start()
    {
        Physics2D.IgnoreCollision(shotAuthor.GetComponent<Collider2D>(), gameObject.GetComponent<Collider2D>());
        transform.Rotate(0, 0, -90);
        StartCoroutine(expire());
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += transform.up * projectileSpeed * Time.deltaTime;
    }

    IEnumerator expire()
    {
        yield return new WaitForSeconds(expirationLength);
        Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.CompareTag("Player"))
        {
            collision.transform.GetComponent<PlayerHealth>().getHit(damage);
        }
        Destroy(gameObject);
    }
}
