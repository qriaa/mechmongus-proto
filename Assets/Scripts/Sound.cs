using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sound : MonoBehaviour
{
    public AudioClip audioClip;
    private AudioSource audioSource;
    bool hasPlayed = false;
    // Start is called before the first frame update
    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void playSoundAndDie()
    {
        float volumeLevel = VolumeSettings.GetVolumeLevel();
        audioSource.volume = volumeLevel;
        audioSource.PlayOneShot(audioClip);
        hasPlayed = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (hasPlayed && !audioSource.isPlaying)
        {
            Destroy(gameObject);
        }
    }
}
